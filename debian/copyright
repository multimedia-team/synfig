Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: synfig
Source: http://sourceforge.net/projects/synfig/
Files-Excluded:
    .github
    gtkmm-osx
    synfig-osx
    autobuild/macports
    synfig-studio
Comment:
 This package was debianized by Paul Wise <pabs@debian.org> on 2005-11-05

Files: *
Copyright: 2002-2005      Adrian Bentley
           2010           Brendon Higgins
           2008-2014      Carlos López
           2009           Carlos A. Sosa Navarro
           2007-2008      Chris Moore
           2009-2015      Diego Barrios Romero
           2008           Gerco Ballintijn
           2009,2012-2013 Konstantin Dmitriev
           2009-2010,2012 Nikita Kitaev
           2006-2008      Paul Wise
           2009           Ray Frederikson
           2002-2005      Robert B. Quattlebaum Jr.
           2015           Jérôme Blanchi
           2013           Moritz Grosch (LittleFox) <littlefox@fsfe.org>
           2015           Denis Zdorovtsov (mrtrizer) <mrtrizer@gmail.com>
           2013-2019      Ivan Mahonin
License: GPL-2+

Files: debian/*
Copyright: 2012-2022 Dmitry Smirnov <onlyjob@debian.org>
           2007-2009 Cyril Brulebois <kibi@debian.org>
           2006      Fabian Fagerholm <fabbe@debian.org>
           2006      Miguel Gea Milvaques <xerakko@debian.org>
           2005-2009 Paul Wise <pabs@debian.org>
           2009      Scott Kitterman <scott@kitterman.com>
           2010      Stefano Zacchiroli <zack@debian.org>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.
